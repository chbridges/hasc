#include <iostream>
#include <vector>
#include <thread>
#include <numeric>  // std::iota
#include <cstdlib>  // atoi

// double precision alpha * x + y
void daxpy(const double alpha, const std::vector<double> x, std::vector<double> &y, const int begin, const int end)
{
    for (int i = begin; i < end; ++i)
        y[i] = alpha * x[i] + y[i];
}

int main(int argc, char * argv[])
{
    // Thread & problem sizes
    const int n_threads = std::thread::hardware_concurrency();
    const int N = (argc > 1) ? atoi(argv[1]) : 512;
    const int partial_size = (N + n_threads - 1) / n_threads;

    // Parameter initialization
    const double alpha = -2.0;
    std::vector<double> x(N);
    std::vector<double> y(N, 1.0);
    std::iota(x.begin(), x.end(), 0);   // Fill x with 0, ... , N-1
    // alpha * x = 0, -2, -4, -6, -8, ...
    // y[i] <- -2.0 * i + 1
    // 1, -1, -3, -5, -7, ...

    // Outer scope will deallocate all intermediate vectors and variables
    {   
        std::vector<std::thread> threads;
        int begin, end;

        // std::min ensures that we don't use n threads for vectors of size < n
        for (int rank = 0; rank < std::min(n_threads, N); ++rank)
        {
            begin = rank * partial_size;
            end = std::min((rank+1) * partial_size, (int) x.size());
            
            threads.push_back(
                std::thread(daxpy, alpha, x, std::ref(y), begin, end)
            );
        }

        for (auto& thread : threads)
            thread.join();
    }

    // Only for N <= 20 all elements will be printed, otherwise
    // first 10 elements, ellipse, last 10 elements
    if (N > 20)
    {
        for (int i = 0; i < std::min(N-10, 10); ++i)
            std::cout << y[i] << " ";
        std::cout << " ... ";
        for (int i = std::max(N-10, 10); i < N; ++i)
            std::cout << y[i] << " ";
    }
    else
        for (const auto& el : y)
            std::cout << el << " ";

    std::cout << std::endl;
}