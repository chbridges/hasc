#include <iostream>
#include <vector>
#include <functional>
#include <random>
#include "time_experiment.hh"

double global_var = 0.0;

class Experiment
{
    private:
        const std::function<void(size_t)> func;
        const size_t n;

    public:
        Experiment() = delete;
        
        Experiment(std::function<void(size_t)> func, size_t n)
            : func(func), n(n)
        {}

        void run() const
        {
            func(n);
        }
};

void empty_loop(size_t n)
{
    for(int i = 0; i < n; ++i);
}

void accumulating_loop(size_t n)
{
    std::mt19937 generator(0);
    std::uniform_real_distribution<double> dist(-1.0, 1.0);
    global_var = 0.0;

    for(int i = 0; i < n; ++i)
        global_var += dist(generator);
}

void vector_loop(size_t n)
{
    std::vector<size_t> vec(n);

    for(int i = 0; i < n; ++i)
        vec.at(i) = i;
}

int main()
{
    const size_t n = 1024;

    const Experiment empty(empty_loop, n);
    const Experiment accumulating(accumulating_loop, n);
    const Experiment vector(vector_loop, n);

    std::cout << "Measuring mean runtime of empty loop...\n";
    auto t = time_experiment(empty);
    std::cout <<  t.second / t.first << " µs\n\n";
    std::cout << "Measuring mean runtime of accumulating loop...\n";
    t = time_experiment(accumulating);
    std::cout <<  t.second / t.first << " µs\n\n";
    std::cout << "Measuring mean runtime of vector loop...\n";
    t = time_experiment(vector);
    std::cout << t.second / t.first << " µs" << std::endl;
}