/** ex3 version with support for Vec8f and float scalars
 *  (see "using" lines below includes and lines marked with "#Vec8f" and "#Vec4d")
 * Compile with
 *   g++ ex3-vec8.cc -o ex3-vec8 -std=c++17 -O2 -mavx2 -mfma && ./ex3-vec8
**/

#include <iostream>
#include <functional>
#include <chrono>
#include "time_experiment.hh"
#include "vector/vectorclass.h"

// using VEC_T = Vec4d;
using VEC_T = Vec8f;

using SCALAR_T = float;

// The midpoint rule for estimating a definite integral uses a Riemann sum with subintervals of equal width

// x³ - 2x² + 3x - 1, true integral = 1/2 = 0.8333...
template <typename T>
T f1(const T x)
{
    return x*x*x - 2*x*x + 3*x - 1.0f;
}

// x^0 + ... + x^15, true integral = 2436559/720720 = 3.380728993228993
template <typename T>
T f2(const T x)
{
    T sum = 1.0 + x;
    T pow = x; // initialized with x^1
    for (int i = 2; i <= 15; ++i)
    {
        pow *= x;
        sum += pow;
    }
    return sum;
}

// Sequential application of the midpoint rule
SCALAR_T integrate(std::function<SCALAR_T(SCALAR_T)> f, const unsigned int n)
{
    const SCALAR_T width = 1.0 / (float) n;

    SCALAR_T sum = 0.0;

    for (int i = 0; i < n; ++i)
    {
        SCALAR_T c = ((2*i+1) * width) / 2; // Midpoint
        sum += width * f(c);
    }

    return sum;
}

// Vectorized applicaton of the midpoint rule
SCALAR_T integrate(std::function<VEC_T(VEC_T)> f, const unsigned int n)
{
    const SCALAR_T width = 1.0 / (SCALAR_T) n;
    SCALAR_T sum = 0.0f;

    // for (int i = 0; i < n; i += 4) // #Vec4d
    for (int i = 0; i < n; i += 8) // #Vec8f
    {
        // VEC_T sub(i, i+1, i+2, i+3); // #Vec4d
        VEC_T sub(i, i+1, i+2, i+3, i+4, i+5, i+6, i+7); // #Vec8f
        sub = (2*sub+1) * width / 2;
        sub = width * f(sub);
        // if (n-i < 4) // always false if n is divisible by 4 // #Vec4d
        if (n-i < 8) // always false if n is divisible by 8 // #Vec8f
            sub.cutoff(n-i);
        sum += horizontal_add(sub);
    }

    return sum;
}

// Time measurement
template <typename T>
class Experiment
{
    private:
        const std::function<T(T)> f;
        const unsigned int n;

    public:
        Experiment() = delete;
        
        Experiment(std::function<T(T)> f, const unsigned int n)
            : f(f), n(n)
        {}

        void run() const
        {
            integrate(f, n);
        }
};

void measure_time(const unsigned int n)
{
    Experiment<SCALAR_T> e1_seq(f1<SCALAR_T>, n);
    Experiment<VEC_T> e1_vec(f1<VEC_T>, n);
    Experiment<SCALAR_T> e2_seq(f2<SCALAR_T>, n);
    Experiment<VEC_T> e2_vec(f2<VEC_T>, n);

    std::cout << "N = " << n << std::endl;

    auto t = time_experiment(e1_seq);
    std::cout << "f1 sequential: " << t.second / t.first << " µs\n";
    t = time_experiment(e1_vec);
    std::cout << "f1 vectorized: " << t.second / t.first << " µs\n";
    t = time_experiment(e2_seq);
    std::cout << "f2 sequential: " << t.second / t.first << " µs\n";
    t = time_experiment(e2_vec);
    std::cout << "f2 vectorized: " << t.second / t.first << " µs\n" << std::endl;
}

int main()
{
    /*
    std::cout << integrate(f1<SCALAR_T>, 1024) << "\n"
              << integrate(f1<VEC_T>, 1024) << "\n"
              << integrate(f2_seq, 1024) << "\n"
              << integrate(f2_vec, 1024) << std::endl;
    */
    for (int i = 64; i < 65537; i *= 2)
        measure_time(i);
}
