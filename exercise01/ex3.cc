#include <iostream>
#include <functional>
#include <chrono>
#include "time_experiment.hh"
#include "vector/vectorclass.h"

// The midpoint rule for estimating a definite integral uses a Riemann sum with subintervals of equal width

// x³ - 2x² + 3x - 1, true integral = 1/2 = 0.8333...
template <typename T>
T f1(const T x)
{
    return x*x*x - 2*x*x + 3*x - 1.0;
}

// x^0 + ... + x^15, true integral = 2436559/720720 = 3.380728993228993
template <typename T>
T f2(const T x)
{
    T sum = 1.0 + x;
    T pow = x; // initialized with x^1
    for (int i = 2; i <= 15; ++i)
    {
        pow *= x;
        sum += pow;
    }
    return sum;
}

// Sequential application of the midpoint rule
double integrate(std::function<double(double)> f, const unsigned int n)
{
    const double width = 1.0 / (double) n;

    double sum = 0.0;

    for (int i = 0; i < n; ++i)
    {
        double c = ((2*i+1) * width) / 2;   // Midpoint
        sum += width * f(c);
    }

    return sum;
}

// Vectorized applicaton of the midpoint rule
double integrate(std::function<Vec4d(Vec4d)> f, const unsigned int n)
{
    const double width = 1.0 / (double) n;
    double sum = 0.0;

    for (int i = 0; i < n; i += 4)
    {
        Vec4d sub(i, i+1, i+2, i+3);
        sub = (2*sub+1) * width / 2;
        sub = width * f(sub);
        if (n-i < 4)    // always false if n is divisible by 4
            sub.cutoff(n-i);
        sum += horizontal_add(sub);
    }

    return sum;
}

// Time measurement
template <typename T>
class Experiment
{
    private:
        const std::function<T(T)> f;
        const unsigned int n;

    public:
        Experiment() = delete;
        
        Experiment(std::function<T(T)> f, const unsigned int n)
            : f(f), n(n)
        {}

        void run() const
        {
            integrate(f, n);
        }
};

void measure_time(const unsigned int n)
{
    Experiment<double> e1_seq(f1<double>, n);
    Experiment<Vec4d> e1_vec(f1<Vec4d>, n);
    Experiment<double> e2_seq(f2<double>, n);
    Experiment<Vec4d> e2_vec(f2<Vec4d>, n);

    std::cout << "N = " << n << std::endl;

    auto t = time_experiment(e1_seq);
    std::cout << "f1 sequential: " << t.second / t.first << " µs\n";
    t = time_experiment(e1_vec);
    std::cout << "f1 vectorized: " << t.second / t.first << " µs\n";
    t = time_experiment(e2_seq);
    std::cout << "f2 sequential: " << t.second / t.first << " µs\n";
    t = time_experiment(e2_vec);
    std::cout << "f2 vectorized: " << t.second / t.first << " µs\n" << std::endl;
}

int main()
{
    /*
    std::cout << integrate(f1<double>, 1024) << "\n"
              << integrate(f1<Vec4d>, 1024) << "\n"
              << integrate(f2_seq, 1024) << "\n"
              << integrate(f2_vec, 1024) << std::endl;
    */
    for (int i = 32; i < 65537; i *= 2)
        measure_time(i);
}