#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <string>
#include <vector>

#include "nbody_io.hh"
#include "nbody_generate.hh"
#include "time_experiment.hh"
#include "vector/vectorclass.h"

// basic data type for position, velocity, acceleration
typedef double double3[4]; // pad up for later use with SIMD

/*const double gamma = 6.674E-11;*/
const double G = 1.0;
const double epsilon2 = 1E-10;

/** \brief compute acceleration vector from position and masses
 * 
 * Executes \sum_{i=0}^{n-1} (n-i-1)*26 = n(n-1)*13
 * flops including 1 division and one square root
 */
void acceleration (int n, std::vector<Vec4d> x, double m[], std::vector<Vec4d> &a)
{
  for (int i=0; i<n; i++)
    for (int j=i+1; j<n; j++)
      {
        Vec4d d = x[j]-x[i];
        double r2 = horizontal_add(square(d)) + epsilon2;
        double r = sqrt(r2);
        double invfact = G/(r*r2);
        double factori = m[i]*invfact;
        double factorj = m[j]*invfact;
        a[i] += factorj*d;
        a[j] -= factori*d;
      }
}

/** \brief do one time step with leapfrog
 *
 * does n*(n-1)*13 + 12n flops
 */
void leapfrog (int n, double dt, std::vector<Vec4d> &x, std::vector<Vec4d> &v, double m[], std::vector<Vec4d> &a)
{
  // update position: 6n flops
  for (int i=0; i<n; i++)
    x[i] += dt*v[i];

  // save and clear acceleration
  for (int i=0; i<n; i++)
    a[i] = 0.0;
  
  // compute new acceleration: n*(n-1)*13 flops
  acceleration(n,x,m,a);

  // update velocity: 6n flops
  for (int i=0; i<n; i++)
    v[i] += dt*a[i];
}


// Energy conservation
double E_potential(int n, std::vector<Vec4d> x, double m[])
{
  double E_pot = 0.0;
  double m_j;
  double norm;
  Vec4d x_j;
  Vec4d d;

  for (int j=0; j<n; j++)
    {
      m_j = m[j];
      x_j = x[j];

      for (int i=0; i<n; i++)
        {
          if (i==j) continue;

          d = x_j - x[i];
          norm = std::sqrt(horizontal_add(square(d)));
          E_pot += m[i] * m_j / norm;
        }
    }

  return -0.5 * G * E_pot;
}

double E_kinetic(int n, std::vector<Vec4d> v, double m[])
{
  double E_kin = 0.0;
  double norm2 = 0.0;

  for (int i=0; i<n; i++)
    {
      norm2 = horizontal_add(square(v[i]));
      E_kin += m[i] * norm2;
    }

  return 0.5 * E_kin;
}

double E_total(int n, std::vector<Vec4d> x, std::vector<Vec4d> v, double m[])
{
  return E_potential(n, x, m) + E_kinetic(n, v, m);
}


int main (int argc, char** argv)
{
  int n;              // number of bodies in the system
  double *m;          // array for masses
  std::vector<Vec4d> x;           // array for positions
  std::vector<Vec4d> v;           // array for velocites
  std::vector<Vec4d> a;           // array for accelerations
  int timesteps;      // final time step number
  int k;              // time step number
  int mod;            // files are written when k is a multiple of mod 
  char basename[256]; // common part of file name
  char name[256];     // filename with number 
  FILE *file;         // C style file hande
  double t;           // current time
  double dt;          // time step
  double3 *x_sisd;    // for io and generation
  double3 *v_sisd;    // for io and generation

  // command line for restarting
  if (argc==5)
    {
      sscanf(argv[1],"%s",&basename);
      sscanf(argv[2],"%d",&k);
      sscanf(argv[3],"%d",&timesteps);
      sscanf(argv[4],"%d",&mod);
    }
  else if (argc==6) // command line for starting with initial condition
    {
      sscanf(argv[1],"%s",&basename);
      sscanf(argv[2],"%d",&n);
      sscanf(argv[3],"%d",&timesteps);
      sscanf(argv[4],"%lg",&dt);
      sscanf(argv[5],"%d",&mod);
    }
  else // invalid command line, print usage
    {
      std::cout << "usage: " << std::endl;
      std::cout << "nbody_simd <basename> <load step> <final step> <every>" << std::endl;
      std::cout << "nbody_simd <basename> <nbodies> <timesteps> <timestep> <every>" << std::endl;
      return 1;
    }
  
  // set up computation from file
  if (argc==5)
    {
      sprintf(name,"%s_%06d.vtk",basename,k);
      file = fopen(name,"r");
      if (file==NULL)
        {
          std::cout << "could not open file " << std::string(basename) << " aborting" << std::endl;
          return 1;
        }
      n = get_vtk_numbodies(file);
      rewind(file);
      x_sisd = static_cast<double3*>(calloc(n,sizeof(double3)));
      v_sisd = static_cast<double3*>(calloc(n,sizeof(double3)));
      m = static_cast<double*>(calloc(n,sizeof(double)));
      read_vtk_file_double(file,n,x_sisd,v_sisd,m,&t,&dt);
      fclose(file);
      k *= mod; // adjust step number
      std::cout << "loaded " << n << "bodies from file " << std::string(basename) << std::endl;
    }
  // set up computation from initial condition
  if (argc==6)
    {
      x_sisd = static_cast<double3*>(calloc(n,sizeof(double3)));
      v_sisd = static_cast<double3*>(calloc(n,sizeof(double3)));
      m = static_cast<double*>(calloc(n,sizeof(double)));
      //plummer(n,17,x,v,m);
      two_plummer(n,17,x_sisd,v_sisd,m);
      //cube(n,17,1.0,100.0,0.1,x,v,m);
      std::cout << "initialized " << n << " bodies" << std::endl;
      k = 0;
      t = 0.0;
      printf("writing %s_%06d.vtk \n",basename,k);
      sprintf(name,"%s_%06d.vtk",basename,k);
      file = fopen(name,"w");
      write_vtk_file_double(file,n,x_sisd,v_sisd,m,t,dt);
      fclose(file);
    }

  // allocate acceleration vector
  a.resize(n);

  // initialize SIMD vectors
  x.resize(n);
  v.resize(n);

  for (int i = 0; i < n; ++i)
  {
    x[i].load(x_sisd[i]);
    v[i].load(v_sisd[i]);
  }

  // initialize timestep and write first file
  std::cout << "step=" << k << " finalstep=" << timesteps << " time=" << t << " dt=" << dt << std::endl;
  auto start = get_time_stamp();

  // do time steps
  std::vector<double> E_tot;
  E_tot.push_back(E_total(n, x, v, m));

  k += 1;
  for (; k<=timesteps; k++)
    {
      leapfrog(n,dt,x,v,m,a);
      t += dt;
      if (k%mod==0)
        {
          auto stop = get_time_stamp();
          E_tot.push_back(E_total(n, x, v, m));
          double elapsed = get_duration_seconds(start,stop);
          double flop = mod*(13.0*n*(n-1.0)+12.0*n);
          printf("%g seconds for %g ops = %g GFLOPS\n",elapsed,flop,flop/elapsed/1E9);

          printf("writing %s_%06d.vtk \n",basename,k/mod);                 
          sprintf(name,"%s_%06d.vtk",basename,k/mod);
          file = fopen(name,"w");

          for(int i = 0; i < n; ++i)
          {
            x[i].store(x_sisd[i]);
            v[i].store(v_sisd[i]);
          }

          write_vtk_file_double(file,n,x_sisd,v_sisd,m,t,dt);
          fclose(file);
                  
          start = get_time_stamp();
        }
    }

  std::cout << "\nTotal energy (dt in first column):\n" << dt;
  for (const auto& E : E_tot)
    std::cout << ',' << E;
  std::cout << std::endl;

  return 0;
}
