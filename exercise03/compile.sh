g++ -std=c++17 nbody_vanilla.cc -o nbody_vanilla -O2
g++ -std=c++17 nbody_simd.cc -o nbody_simd -O2 -mavx2 -mfma
g++ -std=c++17 nbody_thread.cc -o nbody-thread -O2 -mavx2 -mfma -pthread
